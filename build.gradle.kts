plugins {
    id("com.github.ben-manes.versions") version "0.28.0"
    id("com.github.johnrengelman.shadow") version "5.2.0"
    id("ru.endlesscode.bukkitgradle")
}

configureKotlinProject()

version = "0.1"
description = "Insert description of plugin here"

bukkit {
    version = "1.15.2"

    meta {
        setName("MyPlugin")
    }

    run {
        setCore("spigot")
        eula = true
    }
}

dependencies {
    compileOnly(bukkit) { isTransitive = false }
}

tasks.shadowJar {
    minimize()

    val shadowPackage = "shadow.${project.group}.${project.name}"

    // Kotlin
    relocate("org.jetbrains", "${shadowPackage}.jetbrains")
    relocate("kotlinx", "${shadowPackage}.kotlinx")
    relocate("kotlin", "${shadowPackage}.kotlin")
}
