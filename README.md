## How to use
1. Clone project: 
`> git clone https://gitlab.com/endlesscodegroup/empty-bukkit-plugin.git <PLUGIN_NAME_HERE>`  
2. Change project name and description in `settings.gradle` and `build.gradle`.
3. Change project packages to needed.
4. Done!